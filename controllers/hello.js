var path = require('../lib/Path/path.js');
var fs = require('fs');
var base = require(path.root_dir() + "/lib/Controller/base.js");
var user = require(path.root_dir() + "/models/user.js");
var Hello;

Hello.prototype = new base();

function Hello(request, response) {
    this.request = request;
    this.response = response;
    this.controller_name = "hello";
}

Hello.prototype.world = function world() {
    var foo = "baz";
    this.render({"foo": foo});
}

Hello.prototype.stephen = function stephen() {
    var stephen = new user({"name" : "stephen"});

    // stephen.save(function() {
    //     console.log(stephen);
    // });

    this.render({ "name" : stephen.get('name') });
}

module.exports = Hello;
