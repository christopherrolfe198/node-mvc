var path = require('../lib/Path/path.js');
var fs = require('fs');
var base = require(path.root_dir() + "/lib/Controller/base.js");
var user = require(path.root_dir() + "/models/user.js");
var Home;

Home.prototype = new base();

function Home(request, response) {
    this.request = request;
    this.response = response;
    this.controller_name = "hello";
}

Home.prototype.index = function() {
    this.render();
}

module.exports = Home;
