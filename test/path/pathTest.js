var assert = require("assert");
var path = require(process.cwd() + "/lib/Path/path");

describe('Path', function() {
    describe('#root_dir', function() {
        it('should return the string to the root directory', function() {
            assert.equal(process.cwd(), path.root_dir());
        })
    })

    describe('#config_dir', function() {
        it('should return the string to the config directory', function() {
            assert.equal(process.cwd() + '/config/', path.config_dir());
        })
    })
});
