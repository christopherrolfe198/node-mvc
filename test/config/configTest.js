var assert = require("assert");
var config = require(process.cwd() + "/lib/Config/config");

describe('Config', function() {
    describe('#get', function() {
        it('should return a nodetest when the db config option exists', function() {
            assert.equal('nodetest', config.get('database.db'))
        })
    })
});
