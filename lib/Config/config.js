var path = require('../Path/path.js');

function Config() {
}

Config.prototype.get = function(option) {
    option = option.split(".");

    // try {
        var file = require(path.config_dir() + option[0]);
    // } catch (exception) {
    //     throw new Error("Configuration file not found");
    // }

    if (undefined == file[option[1]]) {
        throw new Error("Configuration option is not defined");
    }

    return file[option[1]];
}

module.exports = new Config;
