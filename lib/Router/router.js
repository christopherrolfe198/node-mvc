var path = require('../Path/path.js');

function Router(request, response) {
    this.request = request;
    this.response = response;
    this.controller = {};
}

Router.prototype.load_route = function() {
    // Determine from route a controller to load
    var full_url = this.request.url;
    if (full_url[0] == '/') { full_url = full_url.substring(1) }
    var url = full_url.split("/");

    var controller_name = url[0];
    var method_name = url[1];
    var args = url.slice(2);

    if (!method_name && !controller_name) {
        method_name = 'index';
        controller_name = 'home';
    }

    this.controller.object = require(path.root_dir() + '/controllers/'+controller_name+'.js');
    var instance = new this['controller']['object'](this.request, this.response);

    if (instance[method_name]) { instance[method_name](); }
    else { this.short_write(501, "Controller method '"+method_name+"' doesn't exist", {}); }
}

Router.prototype.short_write = function(status, message, headers) {
    this.response.writeHead(status, headers);
    this.response.end(message);
}

module.exports = Router;
