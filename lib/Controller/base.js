var path = require('../Path/path');
var fs = require('fs');
var jade = require('jade');

function Base() {
    this.controller_name = this.constructor.name;
}

Base.prototype.render = function(view_vars) {
    var self = this;
    var method = arguments.callee.caller.name;

    fs.readFile(path.root_dir() + "/views/"+this.controller_name+"/"+method+".jade", "utf8", function(err, data) {
        if (data !== undefined) {
            var fn = jade.compile(data);
            var html = fn(view_vars);
            self.response.writeHead(200, {"Content-Type" : "text/html"} );
            self.response.end(html);
        }
        self.response.writeHead(500, {"Content-Type" : "text/html"} );
        self.response.end("<h1>Uh oh</h1><p>We couldn't find that template</p>");
    });
}

module.exports = Base;
