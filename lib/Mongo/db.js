var path = require('../Path/path.js');
var config = require(path.root_dir() + "/lib/Config/config");
var promise = require('es6-promise');

function Db() {
    var self = this;
    this.loaded = false;
    var MongoClient = require('mongodb').MongoClient;
    var db = config.get('database.db');

    this.promise = new promise.Promise(function(resolve, reject) {
        MongoClient.connect("mongodb://localhost:27017/" + db, function(err, db) {
            if (err) {
                reject();
                throw err;
            }

            self.loaded = true;
            self.db = db;
            resolve("Connected");
        });
    })

}

Db.prototype.isConnected = function() {
    if (this.loaded) {
        return true;
    }
    return false;
}

Db.prototype.getConnection = function() {
    if (!this.db) {
        return this.promise;
    }
    return this.db;
}

var dbObj = new Db;

module.exports = dbObj;
