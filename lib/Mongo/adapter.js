var path = require('../Path/path.js');
var db = require(path.root_dir() + "/lib/Mongo/db.js");

function MongoAdapter() {
    this.db = db.getConnection();
}

MongoAdapter.prototype.execute = function(callback) {
    db.promise.then(callback);
}

MongoAdapter.prototype.insert = function(collectionName, properties, callback) {
    var self = this;

    this.execute(function() {
        var collection = self.db.collection(collectionName);
        collection.insert(properties, {w:1}, callback);
    });
}

MongoAdapter.prototype.get = function(collectionName, properties) {
    var self = this;

    this.execute(function() {
        var collection = self.db.collection(collectionName);

        console.log(collection.find(properties));
    })
}

module.exports = new MongoAdapter;
