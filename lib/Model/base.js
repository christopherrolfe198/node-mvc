var path = require('../Path/path.js');
var adapter = require(path.root_dir() + "/lib/Mongo/adapter.js");

function Base() {
    this.properties = {};
    this.modelName = this.constructor.name;
}

Base.prototype.load = function(find) {
    if (Object.keys(this.properties).length > 0) {
        return adapter.get(this.modelName, this.properties);
    }
    throw "No properties specified to load the model in from Mongo"
}

Base.prototype.save = function(callback) {
    var self = this;

    adapter.insert(this.modelName, this.properties, function(err, result) {
        if (err) throw err;
        self.set('_id', result[0]._id);

        callback();
    })
}

Base.prototype.get = function(property) {
    if (!property) return this.properties;
    if (!this.properties[property]) { throw new Error("Property doesn't exist on the model " + this.modelName) }
    return this.properties[property];
}

Base.prototype.set = function(property, value) {
    this.properties[property] = value;
}

module.exports = Base;
