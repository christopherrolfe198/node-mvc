var fs = require('fs');

function Path() {
    var root = '';
}

Path.prototype.root_dir = function() {
    if (this.root) { return this.root; }

    if (fs.existsSync(process.cwd()+'/package.json')) {
        this.root = process.cwd();
    } else {
        var current = process.cwd();

        while(!fs.existsSync(current + '/package.json')) {
            current = current+'/..';
        }

        this.root = current;
    }

    return this.root;
}

Path.prototype.config_dir = function() {
    return this.root_dir() + '/config/';
}

module.exports = new Path;
