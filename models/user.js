var path = require('../lib/Path/path.js')
var base = require(path.root_dir() + "/lib/Model/base.js");
var User;

User.prototype = new base;

function User(properties, find) {
    this.properties = properties;

    this.load();
}

module.exports = User;
