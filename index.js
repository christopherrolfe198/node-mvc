var http = require('http');
var router = require('./lib/Router/router.js');
var port = 8000;

http.createServer(function(request, response) {
    // To be handled in the router eventually
    if (request.url == "/favicon.ico") {return;}

    var routes = new router(request, response);
    routes.load_route();
    // response.end('Hello World\n');
}).listen(port);

console.log("Server running at http://localhost:"+port);
